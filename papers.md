---
title: Papers
layout: default
permalink: /papers/
---

# Google Scholar
[Recent First](https://scholar.google.ca/citations?hl=en&user=JU1Z-DcAAAAJ&view_op=list_works&sortby=pubdate), [Most Cited First](https://scholar.google.ca/citations?user=JU1Z-DcAAAAJ&hl=en)

# Conference
{% bibliography --file conference %}

# Journal
{% bibliography --file journal %}

# Technical Reports
{% bibliography --file techreportandarxiv %}

# Book Chapters
{% bibliography --file chapters %}

# Invited Papers
{% bibliography --file invited %}

# Workshop Publications
{% bibliography --file workshop %}
